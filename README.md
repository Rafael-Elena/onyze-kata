# OnyzekataApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.2.

## Development back-api

Run `npm run server` for a dev server. Navigate to `http://localhost:3000/`. Here you can found data of passengers. This data is called by angular-app. 

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


Remember `npm i`to install node modules in both 