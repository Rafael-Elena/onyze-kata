import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Passengers } from '../../../models/Iinformation';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class InformationService {
  private passengersUrl = 'http://localhost:3000/passengers';

  constructor(private httpClient: HttpClient) {}
  public getPassengers(): Observable<any> {
    return this.httpClient.get(this.passengersUrl).pipe(
      map((response: Passengers[]) => {
        if (!response) {
          throw new Error('Value Expected');
        } else {
          return response;
        }
      }),
      catchError((error) => {
        throw new Error(error.message);
      })
    );
  }
}
