import { InformationService } from './../services/information.service';
import { Passengers } from 'src/app/models/Iinformation';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-information',
  templateUrl: './user-information.component.html',
  styleUrls: ['./user-information.component.scss']
})
export class UserInformationComponent implements OnInit {

  public passengers: Passengers[];

  constructor(public informationService: InformationService) {
  }

  ngOnInit(): void {
    this.getInformationPassengers();
  }

  public getInformationPassengers(): void {
    this.informationService.getPassengers().subscribe((data: Passengers[]) => {
      this.passengers = data;
    }, (error) => {
      console.error(error.message);
    })
  }


}
