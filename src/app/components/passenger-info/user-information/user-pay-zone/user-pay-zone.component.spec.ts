import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPayZoneComponent } from './user-pay-zone.component';

describe('UserPayZoneComponent', () => {
  let component: UserPayZoneComponent;
  let fixture: ComponentFixture<UserPayZoneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserPayZoneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPayZoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
