import { InformationService } from './components/passenger-info/services/information.service';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'; 

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PassengerInfoComponent } from './components/passenger-info/passenger-info.component';
import { UserInformationComponent } from './components/passenger-info/user-information/user-information.component';
import { UserTourComponent } from './components/passenger-info/user-information/user-tour/user-tour.component';
import { UserPayZoneComponent } from './components/passenger-info/user-information/user-pay-zone/user-pay-zone.component';
import { SwitchComponent } from './components/switch/switch.component';
import { HeaderCardComponent } from './components/header-card/header-card.component';

@NgModule({
  declarations: [
    AppComponent,
    PassengerInfoComponent,
    UserInformationComponent,
    UserTourComponent,
    UserPayZoneComponent,
    SwitchComponent,
    HeaderCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [InformationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
